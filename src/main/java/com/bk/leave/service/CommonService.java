package com.bk.leave.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bk.leave.entity.LeaveDetails;
import com.bk.leave.repository.LeaveRepository;

@Service
public class CommonService {
	
	@Autowired
	LeaveRepository rep;

	public void applyLeave(LeaveDetails ld) {
		
		rep.save(ld);
		
	}
	
	
}
